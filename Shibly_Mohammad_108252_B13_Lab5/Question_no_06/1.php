<?php

        //01
        //sort:- Sort an array
        //Syntex:- sort($color);
        //Example
        
        $color = array('white', 'green', 'red', 'blue', 'black');
        echo "<pre>";
        var_dump($color);
        echo "</pre>";
    
        echo "</br>";        
        echo "</br>";
        
            sort($color);
            foreach ($color as $key => $val) {
            echo "color[" . $key . "] = " . $val ;}
            