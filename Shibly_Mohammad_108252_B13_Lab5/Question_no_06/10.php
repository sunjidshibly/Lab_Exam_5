<?php

        //10
        //Array Sum:-Calculate the sum of values in an array
        //Syntex:- array_sum($a);
        //Example



$a = array(1, 2, 3, 4);
echo "sum(a) = " . array_sum($a);

echo "</br>"; 
echo "</br>"; 

$b = array("a" => 1.2, "b" => 2.3, "c" => 3.4);
echo "sum(b) = " . array_sum($b);


?> 
