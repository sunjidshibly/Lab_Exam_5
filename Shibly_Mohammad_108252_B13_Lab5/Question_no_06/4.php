<?php

 //03 
        //prev:- get to the previous array pointer
        //Syntex:- prev($color);
        //Example
        
        
        
$color = array('white', 'green', 'red', 'blue', 'black');
echo $mode = current($color); // $mode = 'white';
echo $mode = next($color);    // $mode = 'green';
echo $mode = next( $color);    // $mode = 'red';
echo $mode = prev( $color);    // $mode = 'green';
echo $mode = end( $color);     // $mode = 'black';
