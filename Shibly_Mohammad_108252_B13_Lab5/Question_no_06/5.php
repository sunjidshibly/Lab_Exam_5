<?php

        //05 
        //list — Assign a variables as they are array.
        //Syntex:- list($name)=$array;
        //Example
        

        $color = array('white', 'green', 'red', 'blue', 'black');

        list($a, $b, $c) = $color;
        echo "$a is my favourite color. $b and $c are favourite too";