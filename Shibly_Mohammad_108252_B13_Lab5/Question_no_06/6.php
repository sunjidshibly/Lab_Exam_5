<?php


        //06 
        //Pop:-Pop the element from the end of an array
        //Syntex:- array_pop($color);
        //Example


$color = array("orange", "red", "black");
echo "<pre>";
print_r($color);
echo "</pre>";

echo "</br>";
array_pop($color);
echo "<pre>";
print_r($color);
echo "</pre>";
