<?php

        //03
        //Reset:- Set the pointer of an array in the first element
        //Syntex:- reset($array);
        //Example
            
$array = array('white', 'green', 'red', 'blue', 'black');


echo current($array) . "<br />"; // "first color"


next($array);
next($array);
echo current($array) . "<br />"; // "third color"

reset($array);
echo current($array) . "<br />"; // "first color"

